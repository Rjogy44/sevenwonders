//
//  Wow.h
//  CollectionView
//
//  Created by Randy Jorgensen on 2/29/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Wow : NSObject
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* image_thumb;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* wikipedia;
@property (nonatomic, strong) NSString* year_built;
@property (nonatomic, strong) NSString* latitude;
@property (nonatomic, strong) NSString* longitude;

@end