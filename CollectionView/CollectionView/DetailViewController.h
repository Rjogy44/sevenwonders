//
//  DetailViewController.h
//  CollectionView
//
//  Created by Randy Jorgensen on 2/29/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Wow.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface DetailViewController : UIViewController

{
    
    
    NSMutableArray* arrayOfSevenWonders;
    NSMutableArray* setArray;
    UIView* sv;
}

@property (nonatomic, strong ) NSString *id1;
@property(nonatomic, strong) NSMutableArray* sevenArray;
@end
@interface sevenArray : NSArray
@end
